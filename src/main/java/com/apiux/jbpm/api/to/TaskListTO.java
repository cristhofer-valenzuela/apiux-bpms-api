package com.apiux.jbpm.api.to;

import java.io.Serializable;
import java.util.List;

import org.kie.services.client.serialization.jaxb.impl.query.JaxbQueryTaskInfo;

public class TaskListTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5840345740079113358L;
	
	private List<JaxbQueryTaskInfo> taskInfo;
	private int total;
	
	public TaskListTO() {
		
	}

	public List<JaxbQueryTaskInfo> getTaskInfo() {
		return taskInfo;
	}

	public void setTaskInfo(List<JaxbQueryTaskInfo> taskInfo) {
		this.taskInfo = taskInfo;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	

}
