package com.apiux.jbpm.api.to;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ConnectionTO implements Serializable {
	
	private static final long serialVersionUID = -4459925891272158618L;
	
	private String userName;
	private String password;
	private String deploymentId;
	private String urlServer;
	
	

	public String getUrlServer() {
		return urlServer;
	}

	public void setUrlServer(String urlServer) {
		this.urlServer = urlServer;
	}

	public ConnectionTO() {
		
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}
}
