package com.apiux.jbpm.api.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.apiux.jbpm.api.exception.RESTException;

public abstract class AbstractREST {

	private static final Logger LOG = Logger.getLogger(AbstractREST.class.getName());
	private static final String AUTH_HEADER = "Authorization";

	public abstract String getBasicAuthentication();

	public HttpResponse getRest(String url) throws RESTException {

		try {
			LOG.info("[get] Inicio....");
			LOG.info("[get] url: " + url);			

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpMethod = new HttpGet(url);

			String auth = this.getBasicAuthentication();

			if(auth == null){
				throw new RESTException("No se encontró una autenticación válida");
			}

			LOG.info("[get] autenticando: " + auth);	
			httpMethod.setHeader(AUTH_HEADER, auth);			

			HttpResponse serverResponse = httpClient.execute(httpMethod);

			LOG.info("[get] Fin");

			return serverResponse;
		} catch (Exception e) {
			String error = "Ocurrio un error al realizar la consulta rest";
			LOG.log(Level.SEVERE, error, e);
			throw new RESTException(error);
		}		
	}
}
