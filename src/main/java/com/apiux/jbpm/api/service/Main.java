package com.apiux.jbpm.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.kie.api.runtime.process.ProcessInstance;
import org.kie.services.client.serialization.jaxb.impl.query.JaxbQueryTaskInfo;
import org.kie.services.client.serialization.jaxb.impl.task.JaxbTaskSummary;

import com.apiux.jbpm.api.to.ConnectionTO;
import com.apiux.jbpm.api.to.TaskListTO;

public class Main extends AbstractBPMSService{

	public static void main(String[] args) {

		Main main = new Main();
		try {

			long time_start, time_end;
			time_start = System.currentTimeMillis();

//					http://161.131.140.64:8080/business-central/rest/query/runtime/task?tid=12274
//					http://161.131.140.64:8080/business-central/rest/query/runtime/task?po=plorca&tst=Ready&tst=InProgress&tst=Created&tst=Reserved


//					JaxbQueryTaskResult 

//			String userBpms = "bpmsAdmin";
//			String userTest = "test";
//
//			String user = userBpms;


//					String server = "http://localhost:8080/business-central";
//					String url = server + "/rest/query/runtime/task?potentialowner=" + user;

//					String url = "http://161.131.140.64:8080/business-central/rest/query/runtime/task?tid=12274";
//					String url = "http://161.131.140.64:8080/business-central/rest/query/runtime/task?po=plorca&tst=Ready&tst=InProgress&tst=Created&tst=Reserved";
//					String url = "http://161.131.140.64:8080/business-central/rest/query/runtime/task?po=plorca&tst=Ready&tst=InProgress&tst=Created&tst=Reserved&p=1&s=10";
//					http://161.131.140.64:8080/business-central/rest/task/query?s=10&p=3

//			Bandeja
//			String url = "http://161.131.140.64:8080/business-central/rest/query/runtime/task?po=plorca&tst=Ready&tst=InProgress&tst=Created&tst=Reserved&p=1&s=10";

//			Tarea por id
//			String url = "http://161.131.140.64:8080/business-central/rest/query/runtime/task?tid=12274";

			
			//					325
			//					
			
//			main.getRest(url);
			
//			main.getTask(url);
			
			
			/**
			 * Set de pruebas
			 */
			
			// Crear proceso
//			ProcessInstance p = main.startProcess("Docux.partOffice", null);			
//			System.out.println("id: " + p.getId());
			
			// Abortar proceso
//			main.abortProcess(2L);
			
			//Obtener processInstance
//			ProcessInstance p = main.getProcessInstance(3L);
//			System.out.println("id: " + p.getId());
			
			// Dejar tarea
//			main.releaseTask(1L, userBpms);
			
			// pedir tarea
//			main.claimTask(1L, userBpms);
			
			// inicia tarea
//			main.startTask(1L, userBpms);
			
			// pide e inicia tarea
//			main.claimAndStartTask(1L, userBpms);
			
			// obtener una tarea
//			JaxbQueryTaskInfo taskInfo = main.getTaskById(12274L);
//			
//			System.out.println("Información de tarea...");
//			for(JaxbTaskSummary task : taskInfo.getTaskSummaries()){
//				System.out.println("id: " + task.getId());
//				System.out.println("name: " + task.getName());
//			}
//			
//			System.out.println("Información de variables...");
//			for(JaxbVariableInfo var : taskInfo.getVariables()){
//				System.out.println("value: " + var.getValue());
//				System.out.println("name: " + var.getName());
//			}
			
			
			// finaliza tarea
//			main.endTask(1L, userBpms, null);
			
			
			// obtener todas las variables de proceso
//			List<VariableInstanceLog> variables = main.getVariableInstances(33L);
//			
//			for(VariableInstanceLog var : variables){
//				System.out.println("id: " + var.getVariableId());
//				System.out.println("value: " + var.getValue());
//			}
			
			// Total de tareas 
//			System.out.println("Total de tareas: " + main.getTaskListCount());
			
//			String page = "1";
//			String pageSize = "0";
//			
//			Map<String, String> params = new HashMap<String, String>();
//			
//			params.put("nombre", "nom");	
//			params.put("nombreStart", "*nomStart");
//			params.put("nombreEnd", "nomEnd*");
//			params.put("edad", "*10*");
//			
//			System.out.println("Información de tareas...");
//			List<JaxbQueryTaskInfo> taskList = main.getTaskListPagination(null, null, params);
//			
//			
//			System.out.println("total: " + taskList.size());
//			
//			for(JaxbQueryTaskInfo taskInfo : taskList){
//				
//				for(JaxbTaskSummary task : taskInfo.getTaskSummaries()){
//            		
//            		System.out.println("id: " + task.getId());
//            		System.out.println("Name: " + task.getName());            		
//                }
//			}
			
			// Lista de tareas y total
//			TaskListTO taskList = main.getTaskListPaginationTotal("1", "1", null);			
//			
//			System.out.println("total: " + taskList.getTotal());
//			
//			for(JaxbQueryTaskInfo taskInfo : taskList.getTaskInfo()){
//				
//				for(JaxbTaskSummary task : taskInfo.getTaskSummaries()){
//            		
//            		System.out.println("id: " + task.getId());
//            		System.out.println("Name: " + task.getName());            		
//                }
//			}
			
			
			// Lista todas las tareas del usuarios sin filtro
//			List<JaxbQueryTaskInfo> taskList = main.getTaskListSimple();		
//			
//			System.out.println("total: " + taskList.size());
//			
//			for(JaxbQueryTaskInfo taskInfo : taskList){
//				
//				for(JaxbTaskSummary task : taskInfo.getTaskSummaries()){
//            		
//            		System.out.println("id: " + task.getId());
//            		System.out.println("Name: " + task.getName());            		
//                }
//			}
			
			// Lista todas las tareas del usuarios con filtro de variables			
//			Map<String, String> params = new HashMap<String, String>();
//			
//			params.put("nombre", "nom");	
//			params.put("edad", "10");
//			
//			List<JaxbQueryTaskInfo> taskList = main.getTaskListVariableFilter(params);		
//			
//			System.out.println("total: " + taskList.size());
//			
//			for(JaxbQueryTaskInfo taskInfo : taskList){
//				
//				for(JaxbTaskSummary task : taskInfo.getTaskSummaries()){
//            		
//            		System.out.println("id: " + task.getId());
//            		System.out.println("Name: " + task.getName());            		
//                }
//			}
//
//
//			time_end = System.currentTimeMillis();
//
//			System.out.println("Fin.....: " + ((time_end - time_start) / 1000));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public ConnectionTO getConnection() {		
		ConnectionTO connTO = new ConnectionTO();
		connTO.setUrlServer("http://localhost:8081/jbpm-console");
		connTO.setDeploymentId("com.apiux:docux:1.0");
		connTO.setUserName("krisv");
		connTO.setPassword("krisv");
		
//		connTO.setUrlServer("http://161.131.140.64:8080/business-central");
//		connTO.setDeploymentId("cl.bci.bpm.chip.tracking:TrackingCHIP:1.0");
//		connTO.setUserName("bpmsAdmin");
//		connTO.setPassword("BciBpms2016.");
		
//		connTO.setUserName("plorca");
//		connTO.setPassword("bci1234.");
		
		return connTO;
	}

}
