package com.apiux.jbpm.api.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.util.Base64;
import org.jbpm.services.task.impl.model.xml.JaxbContent;
import org.jbpm.services.task.impl.model.xml.JaxbTask;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.runtime.manager.audit.NodeInstanceLog;
import org.kie.api.runtime.manager.audit.ProcessInstanceLog;
import org.kie.api.runtime.manager.audit.VariableInstanceLog;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Status;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;
import org.kie.remote.client.jaxb.JaxbTaskSummaryListResponse;
import org.kie.remote.jaxb.gen.JaxbStringObjectPairArray;
import org.kie.remote.jaxb.gen.SetProcessInstanceVariablesCommand;
import org.kie.remote.jaxb.gen.util.JaxbListWrapper;
import org.kie.remote.jaxb.gen.util.JaxbStringObjectPair;
import org.kie.services.client.serialization.jaxb.impl.query.JaxbQueryTaskInfo;
import org.kie.services.client.serialization.jaxb.impl.query.JaxbQueryTaskResult;
import org.kie.services.client.serialization.jaxb.impl.query.JaxbVariableInfo;
import org.kie.services.client.serialization.jaxb.impl.task.JaxbTaskSummary;

import com.apiux.jbpm.api.exception.BPMSException;
import com.apiux.jbpm.api.to.ConnectionTO;
import com.apiux.jbpm.api.to.TaskListTO;

/**
 * Utilitario de servicios para trabajar con BPMS. Utiliza la api remota para
 * conectarse y ejecutar tareas como creación de procesos, inicio de tareas,
 * pedir tareas, etc.
 * <p>
 * También utiliza los servicios rest publicados en la BPMS para obtener
 * información de las tareas, ya que estos métodos están optimizados, con el
 * paginador y agrega las variables dentro de la tarea.
 * <p>
 * 
 * Registro de versiones:
 * <ul>
 * <li>1.0 13/07/2016 Max Moreno Esturillo: Versión inicial.</li>
 * </ul>
 * </p>
 * 
 * @author Amadeus Tsunami <i><b>"El clásico"</b></i>
 * 
 * 
 *
 */
public abstract class AbstractBPMSService extends AbstractREST {

	private static final Logger LOG = Logger.getLogger(AbstractBPMSService.class.getName());

	private final ResourceBundle properties = ResourceBundle.getBundle("global");

	public abstract ConnectionTO getConnection();

	@Override
	public String getBasicAuthentication() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getConnection().getUserName());
		sb.append(":");
		sb.append(this.getConnection().getPassword());

		String auth = Base64.encodeBytes(sb.toString().getBytes());

		return "Basic " + auth;
	}

	private RuntimeEngine getRemoteEngine() throws BPMSException {

		ConnectionTO conn = this.getConnection();

		try {
			return RemoteRuntimeEngineFactory.newRestBuilder().addUrl(new URL(conn.getUrlServer()))
					.addUserName(conn.getUserName()).addPassword(conn.getPassword())
					.addDeploymentId(conn.getDeploymentId()).addExtraJaxbClasses(new Class[] { JaxbListWrapper.class })
					.build();
		} catch (MalformedURLException e) {
			String error = "Ocurrio un error al conectarse a BPMS";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public ProcessInstance startProcess(String processId, Map<String, Object> params) throws BPMSException {
		LOG.info("[startProcess] Inicio");
		LOG.info("[startProcess] processId: " + processId);

		if (processId == null) {
			throw new BPMSException(400, "processId no puede ser nulo");
		}

		ProcessInstance processInstance = this.getKieSession().startProcess(processId, params);

		LOG.info("[startProcess] Fin");

		return processInstance;
	}

	public void abortProcess(Long processInstanceId) throws BPMSException {
		LOG.info("[abortProcess] Inicio");
		LOG.info("[abortProcess] processInstanceId: " + processInstanceId);

		if (processInstanceId == null) {
			throw new BPMSException(400, "processInstanceId no puede ser nulo");
		}

		this.getKieSession().abortProcessInstance(processInstanceId);

		LOG.info("[abortProcess] Fin");
	}

	public ProcessInstance getProcessInstance(Long processInstanceId) throws BPMSException {
		LOG.info("[getProcessInstance] Inicio");
		LOG.info("[getProcessInstance] processInstanceId: " + processInstanceId);

		if (processInstanceId == null) {
			throw new BPMSException(400, "processInstanceId no puede ser nulo");
		}

		ProcessInstance processInstance = this.getKieSession().getProcessInstance(processInstanceId);

		LOG.info("[getProcessInstance] Fin");

		return processInstance;
	}

	public void claimTask(Long taskId) throws BPMSException {
		LOG.info("[claimTask] Inicio");
		LOG.info("[claimTask] taskId: " + taskId);
		LOG.info("[claimTask] userId: " + this.getConnection().getUserName());

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		this.getTaskService().claim(taskId, this.getConnection().getUserName());

		LOG.info("[claimTask] Fin");
	}

	public List<JaxbQueryTaskInfo> getTasks(String page, String pageSize, Long processId) throws BPMSException {
		try {
			LOG.info("[getTasks] Inicio");
			LOG.info("[getTasks] processId: " + processId);
			LOG.info("[getTasks] userId: " + this.getConnection().getUserName());

			if (processId == null) {
				throw new BPMSException(400, "processId no puede ser nulo");
			}

			String url = this.getUrlService("rest.url.task.processid");
			StringBuilder sb = null;

			// Valores por defecto
			page = page != null ? page : "1";
			pageSize = pageSize != null ? pageSize : "0";

			url = String.format(url, page, pageSize, processId);

			HttpResponse serverResponse = this.getRest(url);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbQueryTaskResult.class);
			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());
			JaxbQueryTaskResult resp = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbQueryTaskResult.class)
					.getValue();

			return resp.getTaskInfoList();
		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}

	}

	public void startTask(Long taskId) throws BPMSException {
		LOG.info("[startTask] Inicio");
		LOG.info("[startTask] taskId: " + taskId);
		LOG.info("[startTask] userId: " + this.getConnection().getUserName());

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		this.getTaskService().start(taskId, this.getConnection().getUserName());

		LOG.info("[startTask] Fin");
	}

	public void claimAndStartTask(Long taskId) throws BPMSException {
		LOG.info("[claimAndStartTask] Inicio");

		this.claimTask(taskId);
		this.startTask(taskId);

		LOG.info("[claimAndStartTask] Inicio");
	}

	public void activateTask(Long taskId) throws BPMSException {
		LOG.info("[activateTask] Inicio");
		LOG.info("[activateTask] taskId: " + taskId);
		LOG.info("[activateTask] userId: " + this.getConnection().getUserName());

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		this.getTaskService().activate(taskId, this.getConnection().getUserName());

		LOG.info("[activateTask] Fin");
	}

	public void releaseTask(Long taskId) throws BPMSException {
		LOG.info("[releaseTask] Inicio");
		LOG.info("[releaseTask] taskId: " + taskId);
		LOG.info("[releaseTask] userId: " + this.getConnection().getUserName());

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		this.getTaskService().release(taskId, this.getConnection().getUserName());

		LOG.info("[releaseTask] Fin");
	}

	public void resumeTask(Long taskId) throws BPMSException {
		LOG.info("[resumeTask] Inicio");
		LOG.info("[resumeTask] taskId: " + taskId);
		LOG.info("[resumeTask] userId: " + this.getConnection().getUserName());

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		this.getTaskService().resume(taskId, this.getConnection().getUserName());

		LOG.info("[resumeTask] Fin");
	}

	public void delegateTask(Long taskId, String targetUserId) throws BPMSException {
		LOG.info("[delegateTask] Inicio");
		LOG.info("[delegateTask] taskId: " + taskId);
		LOG.info("[delegateTask] userId: " + this.getConnection().getUserName());
		LOG.info("[delegateTask] targetUserId: " + targetUserId);

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		if (targetUserId == null) {
			throw new BPMSException(400, "targetUserId no puede ser nulo");
		}

		this.getTaskService().delegate(taskId, this.getConnection().getUserName(), targetUserId);

		LOG.info("[delegateTask] Fin");
	}

	public JaxbQueryTaskInfo getRuntimeTaskById(Long taskId) throws BPMSException {
		try {
			LOG.info("[getTaskById] Inicio");
			LOG.info("[getTaskById] taskId: " + taskId);

			if (taskId == null) {
				throw new BPMSException(400, "taskId no puede ser nulo");
			}

			String url = this.getUrlService("rest.url.task.id") + taskId;

			HttpResponse serverResponse = this.getRest(url);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbQueryTaskResult.class);
			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());
			JaxbQueryTaskResult resp = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbQueryTaskResult.class)
					.getValue();

			if (resp.getTaskInfoList().size() < 1) {
				throw new BPMSException(404, "No se encontró la tarea solicitada");
			}

			JaxbQueryTaskInfo task = resp.getTaskInfoList().get(0);

			LOG.info("[getTaskById] Fin");

			return task;

		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public JaxbTask getTaskById(Long taskId) throws BPMSException {
		try {
			LOG.info("[getTaskById] Inicio");
			LOG.info("[getTaskById] taskId: " + taskId);

			if (taskId == null) {
				throw new BPMSException(400, "taskId no puede ser nulo");
			}

			String url = this.getUrlService("rest.url.task") + taskId;

			HttpResponse serverResponse = this.getRest(url);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbTask.class);
			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());
			JaxbTask task = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbTask.class).getValue();

			if (task == null) {
				throw new BPMSException(404, "No se encontró la tarea solicitada");
			}

			LOG.info("[getTaskById] Fin");

			return task;

		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public void endTask(Long taskId, Map<String, Object> data) throws BPMSException {
		LOG.info("[endTask] Inicio");
		LOG.info("[endTask] taskId: " + taskId);
		LOG.info("[endTask] userId: " + this.getConnection().getUserName());

		if (taskId == null) {
			throw new BPMSException(400, "taskId no puede ser nulo");
		}

		this.getTaskService().complete(taskId, this.getConnection().getUserName(), data);

		LOG.info("[endTask] Fin");
	}

	public int getTaskListCount() throws BPMSException {
		try {
			LOG.info("[getTaskListCount] Inicio");
			LOG.info("[getTaskListCount] userId: " + this.getConnection().getUserName());

			String url = this.getUrlService("rest.url.task.count") + this.getConnection().getUserName();

			HttpResponse serverResponse = this.getRest(url);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbTaskSummaryListResponse.class);
			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());
			JaxbTaskSummaryListResponse resp = jaxbTaskContext.createUnmarshaller()
					.unmarshal(source, JaxbTaskSummaryListResponse.class).getValue();

			LOG.info("[getTaskListCount] Total de tareas: " + resp.getResult().size());

			return resp.getResult().size();

		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public Map<String, Object> getVariableTask(Long taskId) throws BPMSException {
		try {
			LOG.info("[getVariableTask] Inicio");
			LOG.info("[getVariableTask] taskId: " + taskId);

			String url = this.getUrlService("rest.url.task.id.content");
			url = String.format(url, taskId);

			LOG.info("[getVariableTask] url: " + url);

			HttpResponse serverResponse = this.getRest(url);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbContent.class);
			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());
			JaxbContent resp = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbContent.class).getValue();

			LOG.info("[getVariableTask] Total de variables: " + resp.getContentMap().size());

			return resp.getContentMap();

		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public List<JaxbQueryTaskInfo> getTaskListSimple() throws BPMSException {
		return this.getTaskListPagination(null, null, null);
	}

	public List<JaxbQueryTaskInfo> getTaskListVariableFilter(Map<String, String> variableFilter) throws BPMSException {
		return this.getTaskListPagination(null, null, variableFilter);
	}

	public List<JaxbQueryTaskInfo> getTaskListPagination(String page, String pageSize,
			Map<String, String> variableFilter) throws BPMSException {
		try {
			LOG.info("[getTaskListPagination] Inicio");
			LOG.info("[getTaskListPagination] userId: " + this.getConnection().getUserName());

			String url = this.getUrlService("rest.url.task.full");
			StringBuilder sb = null;

			// Valores por defecto
			page = page != null ? page : "1";
			pageSize = pageSize != null ? pageSize : "0";

			url = String.format(url, page, pageSize, this.getConnection().getUserName());

			if (variableFilter != null) {

				for (Entry<String, String> var : variableFilter.entrySet()) {

					sb = new StringBuilder("&");
					sb.append(var.getValue().indexOf("*") == -1 ? "var_" : "vr_");
					sb.append(var.getKey());
					sb.append("=");
					sb.append(var.getValue());
					url += sb.toString();
				}
			}

			HttpResponse serverResponse = this.getRest(url);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbQueryTaskResult.class);
			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());
			JaxbQueryTaskResult resp = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbQueryTaskResult.class)
					.getValue();

			return resp.getTaskInfoList();

		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public TaskListTO getTaskListPaginationTotal(String page, String pageSize, Map<String, String> variableFilter)
			throws BPMSException {

		TaskListTO taskListTO = new TaskListTO();
		taskListTO.setTaskInfo(this.getTaskListPagination(page, pageSize, variableFilter));
		taskListTO.setTotal(this.getTaskListCount());

		return taskListTO;

	}

	@SuppressWarnings("unchecked")
	public List<VariableInstanceLog> getVariableInstances(Long processInstanceId) throws BPMSException {
		LOG.info("[getVariableInstances] Inicio");
		LOG.info("[getVariableInstances] processInstanceId: " + processInstanceId);

		if (processInstanceId == null) {
			throw new BPMSException(400, "processInstanceId no puede ser nulo");
		}

		List<VariableInstanceLog> variableList = (List<VariableInstanceLog>) this.getAuditService()
				.findVariableInstances(processInstanceId);

		LOG.info("[getVariableInstances] Fin");

		return variableList;
	}

	public Map<String, VariableInstanceLog> getVariableInstancesAsMap(Long processInstanceId) throws BPMSException {
		LOG.info("[getVariableInstancesAsMap] Inicio");
		LOG.info("[getVariableInstancesAsMap] processInstanceId: " + processInstanceId);

		if (processInstanceId == null) {
			throw new BPMSException(400, "processInstanceId no puede ser nulo");
		}

		Map<String, VariableInstanceLog> params = new HashMap<String, VariableInstanceLog>();

		for (VariableInstanceLog map : this.getVariableInstances(processInstanceId)) {
			params.put(map.getVariableId(), map);
		}

		LOG.info("[getVariableInstancesAsMap] Fin");

		return params;
	}

	public void setVariables(Long processId, Map<String, Object> map) throws BPMSException {
		LOG.log(Level.FINEST, "[setVariables][BCI_INI] Inicio metodo.");
		LOG.log(Level.FINEST, "[setVariables] Se estableceran valores para el proceso con PID: [{0}]", processId);
		
		JaxbStringObjectPairArray variables = new JaxbStringObjectPairArray();
		List<JaxbStringObjectPair> items = variables.getItems();
		
		for (Entry<String, Object> entry : map.entrySet()) {
			Object value = entry.getValue();
			if (value != null) {
				LOG.log(Level.FINEST, "[setVariables] Se establece el valor [{0}] par la variable [{1}].",
						new Object[] { entry.getValue(), entry.getKey() });
				JaxbStringObjectPair pair = new JaxbStringObjectPair();
				pair.setKey(entry.getKey());
				pair.setValue(entry.getValue());
				items.add(pair);
			}
		}
		SetProcessInstanceVariablesCommand cmd = new SetProcessInstanceVariablesCommand();
		cmd.setProcessInstanceId(processId);
		cmd.setVariables(variables);
		this.getKieSession().execute(cmd);
		LOG.log(Level.FINEST, "[setVariables][BCI_FINOK] Fin metodo.");
	}
	
	public List<? extends ProcessInstanceLog> getAllProcess(String processId) throws BPMSException{
		return getAuditService().findProcessInstances(processId);
	}

	private List<Status> getDefaultStatus() {
		List<Status> status = new ArrayList<Status>(4);
		status.add(Status.InProgress);
		status.add(Status.Ready);
		status.add(Status.Created);
		status.add(Status.Reserved);

		return status;
	}

	private String getUrlService(String key) {
		return this.getConnection().getUrlServer() + this.properties.getString(key);
	}

	private KieSession getKieSession() throws BPMSException {
		return this.getRemoteEngine().getKieSession();
	}

	private TaskService getTaskService() throws BPMSException {
		return this.getRemoteEngine().getTaskService();
	}

	private AuditService getAuditService() throws BPMSException {
		return this.getRemoteEngine().getAuditService();
	}

	// private ClientRequest createRequest(URL address) {
	// ClientRequest clientRequest = null;
	// clientRequest = getClientRequestFactory().createRequest(
	// address.toExternalForm());
	// return clientRequest;
	// }
	//
	// private ClientRequestFactory getClientRequestFactory() {
	// ClientRequest clientRequest = null;
	// ClientRequestFactory requestFactory = null;
	// try {
	// DefaultHttpClient httpClient = new DefaultHttpClient();
	// httpClient.getCredentialsProvider().setCredentials(
	// new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT,
	// AuthScope.ANY_REALM),
	// new UsernamePasswordCredentials(username, password));
	// ClientExecutor clientExecutor = new
	// ApacheHttpClient4Executor(httpClient);
	// requestFactory = new ClientRequestFactory(clientExecutor,
	// ResteasyProviderFactory.getInstance());
	// }
	// catch (Exception e) {
	// logger.log(Level.SEVERE, "Error Conexion getClientRequestFactory: " + e);
	// }
	// return requestFactory;
	// }

	public void getTask(String url) {

		try {
			LOG.info("Inicio....");
			LOG.info("url: " + url);

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpMethod = new HttpGet(url);
			// httpMethod.setHeader("Authorization", "Basic
			// cGxvcmNhOmJjaTEyMzQu");
			httpMethod.setHeader("Authorization", this.getBasicAuthentication());

			HttpResponse serverResponse = httpClient.execute(httpMethod);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbQueryTaskResult.class);

			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());

			JaxbQueryTaskResult resp = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbQueryTaskResult.class)
					.getValue();

			// LOG.info("id: " +
			// resp.getTaskInfoList().get(0).getTaskSummaries().get(0).getId());

			System.out.println("Listando tareas...");

			System.out.println("Tareas size: " + resp.getTaskInfoList().size());

			for (JaxbQueryTaskInfo taskInfo : resp.getTaskInfoList()) {

				System.out.println("Tareas resp size: " + taskInfo.getTaskSummaries().size());

				for (JaxbTaskSummary task : taskInfo.getTaskSummaries()) {

					System.out.println("id: " + task.getId());
					System.out.println("Name: " + task.getName());
				}

				// LOG.info("Variables...");

				for (JaxbVariableInfo var : taskInfo.getVariables()) {

					// System.out.println("value: " + var.getValue());
					// System.out.println("Name: " + var.getName());
				}
			}

			// LOG.info("name: " + resp.getName());

			// String resp = EntityUtils.toString(serverResponse.getEntity());
			// LOG.info("Resp: " + resp);

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error", e);
		}
	}

	public List<JaxbTask> getTaskByProcessId(Long processId) throws BPMSException {
		LOG.info("[getTaskByProcessId] inicio");
		LOG.info("[getTaskByProcessId] processId: " + processId);
		List<JaxbTask> listTask = new ArrayList<JaxbTask>();
		List<? extends NodeInstanceLog> nodes = this.getAuditService().findNodeInstances(processId);
		try {
			for (NodeInstanceLog node : nodes) {
				if (node.getNodeType().equals("HumanTaskNode") && node.getType() == NodeInstanceLog.TYPE_ENTER) {
					if (node.getWorkItemId() != null) {
						listTask.add(getTaskById((node.getWorkItemId())));
					}
				}
			}

			return listTask;

		} catch (Exception e) {
			String error = "Ocurrio un error al buscar la tarea";
			LOG.log(Level.SEVERE, error, e);
			throw new BPMSException(error);
		}
	}

	public void getRest2(String url) {

		try {
			LOG.info("Inicio....");
			LOG.info("url: " + url);

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpMethod = new HttpGet(url);
			// httpMethod.setHeader("Authorization", "Basic
			// cGxvcmNhOmJjaTEyMzQu");

			String auth = this.getBasicAuthentication();

			httpMethod.setHeader("Authorization", auth);

			System.out.println("AUTH: " + auth);

			// KieSession ksession =

			HttpResponse serverResponse = httpClient.execute(httpMethod);

			JAXBContext jaxbTaskContext = JAXBContext.newInstance(JaxbQueryTaskResult.class);

			StreamSource source = new StreamSource(serverResponse.getEntity().getContent());

			JaxbQueryTaskResult resp = jaxbTaskContext.createUnmarshaller().unmarshal(source, JaxbQueryTaskResult.class)
					.getValue();

			// LOG.info("id: " +
			// resp.getTaskInfoList().get(0).getTaskSummaries().get(0).getId());

			System.out.println("Listando tareas...");

			System.out.println("Tareas size: " + resp.getTaskInfoList().size());

			for (JaxbQueryTaskInfo taskInfo : resp.getTaskInfoList()) {

				System.out.println("Tareas resp size: " + taskInfo.getTaskSummaries().size());

				for (JaxbTaskSummary task : taskInfo.getTaskSummaries()) {

					System.out.println("id: " + task.getId());
					System.out.println("Name: " + task.getName());
				}

				// LOG.info("Variables...");

				for (JaxbVariableInfo var : taskInfo.getVariables()) {

					// System.out.println("value: " + var.getValue());
					// System.out.println("Name: " + var.getName());
				}
			}

			// LOG.info("name: " + resp.getName());

			// String resp = EntityUtils.toString(serverResponse.getEntity());
			// LOG.info("Resp: " + resp);

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error", e);
		}
	}
}
