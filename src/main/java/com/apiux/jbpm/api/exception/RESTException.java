package com.apiux.jbpm.api.exception;

public class RESTException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4459925891272158618L;
	
	public static final int INTERNAL_ERROR = 500;

	private int status = INTERNAL_ERROR;
	private String response;

	public RESTException() {
		super();
	}
	
	public RESTException(String response) {
		super(response);
		this.response = response;
	}

	public RESTException(int status, String response) {
		super(response);
		this.status = status;
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
